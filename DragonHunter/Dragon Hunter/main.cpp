//
//  main.cpp
//  Dragon Hunter
//
//  Created by Allison Lindner on 15/04/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#include <iostream>
#include "SDL2/SDL.h"
#include "SDL2_image/SDL_image.h"

/***********************************************/
/*********** ASSINATURAS DOS MÉTODOS ***********/
/***********************************************/



/***********************************************/
/***********************************************/

SDL_Window *_window;
SDL_Renderer * _renderer;

int main(int argc, const char * argv[]) {
	
	SDL_Event event;						//Recebe o evento de Mouse e Teclado
	bool quit = false;						//Determina o fim do jogo
	
	SDL_Init(SDL_INIT_EVERYTHING);			//Inicializa a SDL
	
	int imgFlags = IMG_INIT_PNG;
	if( !( IMG_Init( imgFlags ) & imgFlags ) )
	{
		printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
	}
	
	_window = SDL_CreateWindow("DragonHunter",
							   SDL_WINDOWPOS_CENTERED,
							   SDL_WINDOWPOS_CENTERED,
							   1280, 800,
							   SDL_WINDOW_FULLSCREEN_DESKTOP);
	
	_renderer = SDL_CreateRenderer( _window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
	
	SDL_Surface * HUD_HP_Surface;										//Surface da barra de HP
	SDL_Texture * HUD_HP_Texture;										//Textura da barra de HP
	
	SDL_Rect HUD_HP_Rect_Source = { 0 , 0 , 300 , 20 };
	SDL_Rect HUD_HP_Rect_Destiny = { 0 , 0 , 300 , 20 };
	
	
	HUD_HP_Surface = IMG_Load("Images/HUD/HUD_HP.png");
	
	if( HUD_HP_Surface == NULL )
	{
		printf( "Unable to load image. SDL_image Error: %s\n", IMG_GetError() );
	}
	
	HUD_HP_Texture = SDL_CreateTextureFromSurface(_renderer, HUD_HP_Surface);
	HUD_HP_Rect_Source	= { 0 , 20 , 600 , 40 };
	HUD_HP_Rect_Destiny = { 0 , 0 , 300 , 20 };
	
	int frameCount = 0;
	int frameHP = 1;
	int HP = 0;
	
	while (!quit) {
		
		while( SDL_PollEvent( &event ) != 0 ) {
			
			switch (event.type) {
					
				case SDL_QUIT:
					
					quit = true;
					break;
					
				case SDL_KEYDOWN:
					
					switch (event.key.keysym.sym) {
							
						case SDLK_ESCAPE:
							quit = true;
							break;
							
						case SDLK_LEFT:
							HP -= 10;
							if(HP < 0) {
								HP = 0;
							}
							
							std::cout << HP << std::endl;
							
							break;
							
						case SDLK_RIGHT:
							HP += 10;
							if(HP > 100)
								HP = 100;
							
							std::cout << HP << std::endl;
							
							break;
							
						case SDLK_UP:
							
							break;
							
						case SDLK_DOWN:
							
							break;
					}
				break;
			}
		}
		
		frameCount ++;
		int HP_Bar = 0;
		
		if(HP <= 10) {
			HP_Bar = 0;
		} else if(HP > 10 && HP <= 20) {
			HP_Bar = 1;
		} else if(HP > 20 && HP <= 30) {
			HP_Bar = 2;
		} else if(HP > 30 && HP <= 40) {
			HP_Bar = 3;
		} else if(HP > 40 && HP <= 50) {
			HP_Bar = 4;
		} else if(HP > 50 && HP <= 60) {
			HP_Bar = 5;
		} else if(HP > 60 && HP <= 70) {
			HP_Bar = 6;
		} else if(HP > 70 && HP <= 80) {
			HP_Bar = 7;
		} else if(HP > 80 && HP <= 90) {
			HP_Bar = 8;
		} else if(HP > 90 && HP <= 100) {
			HP_Bar = 9;
		} else if(HP == 100) {
			HP_Bar = 10;
		}
		
		HUD_HP_Rect_Destiny = { 300*frameHP , HP_Bar*20 , 300 , 20 };
		
		if(frameCount == 15) {
			frameCount = 0;
			
			if(frameHP == 0) {
				frameHP = 1;
			} else {
				frameHP = 0;
			}
		}
		
		//Render
		
		SDL_UpdateWindowSurface(_window);
		
		SDL_RenderClear(_renderer);
		SDL_RenderCopy(_renderer, HUD_HP_Texture, &HUD_HP_Rect_Destiny, &HUD_HP_Rect_Source );
		SDL_RenderPresent(_renderer);
	}
	
    return 0;
}
